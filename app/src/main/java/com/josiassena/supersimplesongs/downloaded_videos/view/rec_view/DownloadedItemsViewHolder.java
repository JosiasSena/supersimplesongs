package com.josiassena.supersimplesongs.downloaded_videos.view.rec_view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeThumbnailView;
import com.josiassena.supersimplesongs.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * File created by josiassena on 9/11/16.
 */
class DownloadedItemsViewHolder extends RecyclerView.ViewHolder {

    @BindView (R.id.tv_video_title)
    TextView videoTitle;

    @BindView (R.id.video_thumbnail)
    YouTubeThumbnailView videoThumbnail;

    DownloadedItemsViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
