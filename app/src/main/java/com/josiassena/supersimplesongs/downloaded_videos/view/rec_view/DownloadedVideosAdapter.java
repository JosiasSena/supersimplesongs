package com.josiassena.supersimplesongs.downloaded_videos.view.rec_view;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.josiassena.supersimplesongs.R;
import com.josiassena.supersimplesongs.helpers.DeveloperKey;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * File created by josiassena on 9/11/16.
 */
public class DownloadedVideosAdapter extends RecyclerView.Adapter<DownloadedItemsViewHolder> {

    private final Activity activity;
    private final ArrayList<File> files = new ArrayList<>();

    public DownloadedVideosAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public DownloadedItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DownloadedItemsViewHolder(LayoutInflater.from(activity)
                .inflate(R.layout.download_item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(final DownloadedItemsViewHolder holder, int position) {
        final File file = files.get(position);

        String fileName = file.getName();
        final String videoId = fileName.substring(0, fileName.indexOf("---"));

        holder.videoTitle.setText(file.getName().replace(videoId + "---", ""));

        holder.videoThumbnail.initialize(DeveloperKey.DEVELOPER_KEY,
                new YouTubeThumbnailView.OnInitializedListener() {

                    @Override
                    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView,
                                                        final YouTubeThumbnailLoader youTubeThumbnailLoader) {
                        youTubeThumbnailLoader.setVideo(videoId);

                        releaseLoader(youTubeThumbnailLoader);
                    }

                    @Override
                    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView,
                                                        YouTubeInitializationResult youTubeInitializationResult) {
                        holder.videoThumbnail.setImageResource(R.drawable.no_thumbnail);
                    }
                });

        setClickListener(holder, file);

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                new AlertDialog.Builder(activity)
                        .setTitle("Remove video")
                        .setMessage("Would you like to remove this video from your downloads list?")
                        .setPositiveButton("Remove", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //noinspection ResultOfMethodCallIgnored
                                file.delete();
                                files.remove(file);
                                notifyItemRemoved(holder.getAdapterPosition());
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();

                return false;
            }
        });
    }

    private void setClickListener(final DownloadedItemsViewHolder holder, final File file) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri pathUri = Uri.parse(file.getAbsolutePath());

                Intent intent = new Intent(Intent.ACTION_VIEW, pathUri);
                intent.setDataAndType(pathUri, "video/mp4");

                try {
                    activity.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(activity, R.string.need_player_for_offline, Toast.LENGTH_LONG)
                            .show();
                }
            }
        });
    }

    private void releaseLoader(final YouTubeThumbnailLoader youTubeThumbnailLoader) {
        youTubeThumbnailLoader.setOnThumbnailLoadedListener(
                new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                    @Override
                    public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView,
                                                  String s) {
                        youTubeThumbnailLoader.release();
                    }

                    @Override
                    public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView,
                                                 YouTubeThumbnailLoader.ErrorReason errorReason) {
                        youTubeThumbnailLoader.release();
                    }
                });
    }

    /**
     * Returns the total number of items in the data set hold by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return files.size();
    }

    public void setFiles(List<File> files) {
        this.files.addAll(files);
        notifyDataSetChanged();
    }
}
