package com.josiassena.supersimplesongs.downloaded_videos.view;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.josiassena.supersimplesongs.R;
import com.josiassena.supersimplesongs.downloaded_videos.presenter.DownloadedVideosViewPresenterImpl;
import com.josiassena.supersimplesongs.downloaded_videos.view.rec_view.DownloadedVideosAdapter;
import com.josiassena.supersimplesongs.helpers.AdManager;
import com.josiassena.supersimplesongs.helpers.PermissionHelper;
import com.josiassena.supersimplesongs.helpers.dagger.Injector;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DownloadedVideosViewImpl
        extends MvpActivity<DownloadedVideosView, DownloadedVideosViewPresenterImpl>
        implements DownloadedVideosView {

    private static final String TAG = DownloadedVideosViewImpl.class.getSimpleName();

    @BindView (R.id.toolbar)
    Toolbar toolbar;

    @Inject
    PermissionHelper permissionHelper;

    @BindView (R.id.ll_no_vid_view)
    LinearLayout llNoVidView;

    @BindView (R.id.rv_downloaded_videos)
    RecyclerView rvVideos;

    private DownloadedVideosAdapter adapter;

    /**
     * Instantiate a presenter instance
     *
     * @return The {@link MvpPresenter} for this view
     */
    @NonNull
    @Override
    public DownloadedVideosViewPresenterImpl createPresenter () {
        return new DownloadedVideosViewPresenterImpl();
    }

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloaded_videos_view_impl);
        ButterKnife.bind(this);
        Injector.INSTANCE.inject(this);

        setSupportActionBar(toolbar);
        initRecView();
        checkForPermissions();

        AdManager.INSTANCE.displayInterstitialAd();
    }

    private void checkForPermissions () {
        Log.d(TAG, "checkForPermissions() was called");

        String[] permissions = permissionHelper.getPermissions();

        for (String permission : permissions) {
            if (!permissionHelper.isPermissionGranted(this, permission)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(permissionHelper.getPermissions(), 929);
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult (int requestCode, @NonNull String[] permissions,
                                            @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                switch (permissions[0]) {
                    case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                    case Manifest.permission.READ_EXTERNAL_STORAGE:
                        displayPermissionNeedDialog();
                        break;
                    default:
                        Log.e(TAG, "default block of permissions result");
                        break;
                }
            }
        }
    }

    private void displayPermissionNeedDialog () {
        new AlertDialog.Builder(this)
                .setTitle(R.string.storage_perm_req)
                .setMessage(R.string.enable_perm_for_download_access)
                .setPositiveButton(R.string.word_ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick (DialogInterface dialogInterface,
                                                 int i) {
                                permissionHelper.goToAppSettings(DownloadedVideosViewImpl.this);
                            }
                        })
                .setNegativeButton(R.string.word_cancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick (DialogInterface dialogInterface,
                                                 int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
    }

    @Override
    protected void onResume () {
        super.onResume();
        presenter.checkForInternet(this);
    }

    private void initRecView () {
        Log.d(TAG, "initRecView() was called");

        rvVideos.setItemViewCacheSize(50);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvVideos.setLayoutManager(layoutManager);

        adapter = new DownloadedVideosAdapter(this);
        rvVideos.setAdapter(adapter);

        presenter.getDownloadedVideos();
    }

    @Override
    public void onGotFiles (List<File> files) {

        showRecView();

        adapter.setFiles(files);
    }

    private void showRecView () {
        if (llNoVidView != null) {
            llNoVidView.setVisibility(View.GONE);
        }

        if (rvVideos != null) {
            rvVideos.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void displayOfflineWarning () {
        Log.d(TAG, "displayOfflineWarning() was called");

        Snackbar.make(toolbar, R.string.offline_but_vids_available, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void displayNoVideosAvailableView () {
        Log.d(TAG, "displayNoVideosAvailableView() was called");

        if (rvVideos != null) {
            rvVideos.setVisibility(View.GONE);
        }

        if (llNoVidView != null) {
            llNoVidView.setVisibility(View.VISIBLE);
        }
    }
}
