package com.josiassena.supersimplesongs.downloaded_videos.view;

import com.hannesdorfmann.mosby.mvp.MvpView;

import java.io.File;
import java.util.List;

/**
 * File created by josiassena on 9/11/16.
 */
public interface DownloadedVideosView extends MvpView {
    void onGotFiles(List<File> files);

    void displayOfflineWarning();

    void displayNoVideosAvailableView ();
}
