package com.josiassena.supersimplesongs.downloaded_videos.presenter;

import android.app.Activity;
import android.net.NetworkInfo;
import android.util.Log;

import com.github.pwittchen.reactivenetwork.library.Connectivity;
import com.github.pwittchen.reactivenetwork.library.ReactiveNetwork;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.josiassena.supersimplesongs.downloaded_videos.view.DownloadedVideosView;
import com.josiassena.supersimplesongs.helpers.interfaces.OnInternetConnectionListener;

import java.io.File;
import java.util.Arrays;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * File created by josiassena on 9/11/16.
 */
public class DownloadedVideosViewPresenterImpl extends MvpBasePresenter<DownloadedVideosView>
        implements DownloadedVideosViewPresenter, OnInternetConnectionListener {

    private static final String TAG = DownloadedVideosViewPresenterImpl.class.getSimpleName();

    @Override
    public void getDownloadedVideos() {
        Log.d(TAG, "getDownloadedVideos() was called");

        File file = new File("/storage/emulated/0/Download/SuperSimpleSongs");
        File files[] = file.listFiles();

        if (files != null && files.length > 0) {
            if (getView() != null && isViewAttached()) {
                getView().onGotFiles(Arrays.asList(files));
            }
        } else {
            if (getView() != null && isViewAttached()) {
                getView().displayNoVideosAvailableView();
            }
        }
    }

    @Override
    public void checkForInternet(Activity activity) {
        Log.d(TAG, "checkForInternet() was called");

        ReactiveNetwork.observeNetworkConnectivity(activity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Connectivity>() {
                    @Override
                    public void call(Connectivity connectivity) {
                        Log.d(TAG,
                                "call() called with: " + "connectivity = [" +
                                        connectivity.toString() + "]");

                        if (connectivity.getState().equals(NetworkInfo.State.DISCONNECTING) ||
                                connectivity.getState().equals(NetworkInfo.State.DISCONNECTED) ||
                                connectivity.getState().equals(NetworkInfo.State.UNKNOWN)) {
                            if (getView() != null && isViewAttached()) {
                                getView().displayOfflineWarning();
                            }
                        }
                    }
                });
    }
}
