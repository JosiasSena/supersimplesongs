package com.josiassena.supersimplesongs.downloaded_videos.presenter;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.josiassena.supersimplesongs.downloaded_videos.view.DownloadedVideosView;

/**
 * File created by josiassena on 9/11/16.
 */
interface DownloadedVideosViewPresenter extends MvpPresenter<DownloadedVideosView> {
    void getDownloadedVideos();
}
