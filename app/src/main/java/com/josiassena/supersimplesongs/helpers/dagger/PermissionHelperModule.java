package com.josiassena.supersimplesongs.helpers.dagger;

import com.josiassena.supersimplesongs.helpers.PermissionHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * File created by josiassena on 9/12/16.
 */
@Module
public class PermissionHelperModule {

    @Provides
    @Singleton
    PermissionHelper providesPermissionHelper() {
        return new PermissionHelper();
    }

}
