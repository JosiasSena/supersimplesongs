package com.josiassena.supersimplesongs.helpers;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

/**
 * File created by josiassena on 9/11/16.
 */
public class PermissionHelper {

    private final String[] permissions = new String[] {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    /**
     * return true if permission is granted, false otherwise.
     */
    public boolean isPermissionGranted(Context context, @NonNull String permissionsName) {
        return ActivityCompat.checkSelfPermission(context, permissionsName) ==
                PackageManager.PERMISSION_GRANTED;
    }

    public String[] getPermissions() {
        return permissions;
    }

    public void goToAppSettings(Context context) {
        final Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        final Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, uri);
        context.startActivity(intent);
    }
}
