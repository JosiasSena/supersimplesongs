package com.josiassena.supersimplesongs.helpers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.josiassena.supersimplesongs.BuildConfig;
import com.josiassena.supersimplesongs.R;

/**
 * File created by josiassena on 10/10/16.
 */
public enum AdManager {

    INSTANCE;

    private static final String TAG = AdManager.class.getSimpleName();
    private static final String TEST_DEVICE_ID = "5488F09850BD1975DA60CBC79C82E9E4";

    private InterstitialAd interstitialAd;

    public void initAds(Context context) {
        if (!BuildConfig.DEBUG) {
            interstitialAd = new InterstitialAd(context);
            interstitialAd.setAdUnitId(context.getString(R.string.google_interstitial_ad_unit_id));
            interstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    requestNewInterstitial();
                }
            });

            requestNewInterstitial();
        }
    }

    public void displayInterstitialAd() {
        if (interstitialAd != null && interstitialAd.isLoaded() && !BuildConfig.DEBUG) {
            interstitialAd.show();
        } else {
            Log.e(TAG, "Error: Interstitial Ad did not load. " +
                    "Unable to show google interstitial ad content");
        }
    }

    private void requestNewInterstitial() {
        interstitialAd.loadAd(getAdRequest());
    }

    @NonNull
    public AdRequest getAdRequest() {
        return new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(TEST_DEVICE_ID)
                .tagForChildDirectedTreatment(true)
                .build();
    }
}
