package com.josiassena.supersimplesongs.helpers.dagger;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * File created by josiassena on 7/17/16.
 */
@Module
class AppModule {

    private final Application application;

    AppModule(final Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return application;
    }

}

