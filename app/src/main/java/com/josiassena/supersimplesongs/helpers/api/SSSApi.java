package com.josiassena.supersimplesongs.helpers.api;

import com.josiassena.supersimplesongs.models.PlayList;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * File created by josiassena on 7/17/16.
 */
public interface SSSApi {

    @GET("playlistItems?part=snippet&maxResults=50")
    Call<PlayList> getPlayList(@Query("playlistId") String playlistId, @Query("key") String key);

    @GET("playlists?part=snippet&maxResults=50")
    Call<PlayList> getPlayListInfo(@Query("id") String playListId, @Query("key") String key);

}
