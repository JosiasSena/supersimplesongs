package com.josiassena.supersimplesongs.helpers.firebase;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.josiassena.supersimplesongs.helpers.dagger.Injector;

/**
 * File created by josiassena on 10/4/16.
 */
public enum FireBaseRemoteConfigManager {

    INSTANCE;

    private static final String TAG = FireBaseRemoteConfigManager.class.getSimpleName();

    private final FirebaseRemoteConfig firebaseRemoteConfig;

    /**
     * Called when {@link #INSTANCE} is called
     */
    FireBaseRemoteConfigManager() {
        Injector.INSTANCE.inject(this);
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        fetch();
    }

    private void fetch() {
        Log.d(TAG, "fetch() was called");

        long cacheExpiration = 3600; // 1 hour in seconds.

        // If in developer mode cacheExpiration is set to 0 so each fetch will retrieve values from
        // the server.
        if (firebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        // [START fetch_config_with_callback]
        // cacheExpirationSeconds is set to cacheExpiration here, indicating that any previously
        // fetched and cached config would be considered expired because it would have been fetched
        // more than cacheExpiration seconds ago. Thus the next fetch would go to the server unless
        // throttling is in progress. The default expiration duration is 43200 (12 hours).
        firebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.i(TAG, "FirebaseRemoteConfig fetch succeeded!");

                            // Once the config is successfully fetched it must
                            // be activated before newly fetched values are returned.
                            firebaseRemoteConfig.activateFetched();
                        } else {
                            Log.e(TAG, "FirebaseRemoteConfig fetch failed.");
                        }
                    }
                });
        // [END fetch_config_with_callback]
    }
}
