package com.josiassena.supersimplesongs.helpers.dagger;

import android.app.Application;

import com.josiassena.supersimplesongs.App;
import com.josiassena.supersimplesongs.downloaded_videos.view.DownloadedVideosViewImpl;
import com.josiassena.supersimplesongs.helpers.firebase.FireBaseRemoteConfigManager;
import com.josiassena.supersimplesongs.main.presenter.MainPresenterImpl;
import com.josiassena.supersimplesongs.main.view.MainActivity;
import com.josiassena.supersimplesongs.videos.view.VideoViewImpl;
import com.josiassena.supersimplesongs.videos.view.rec_view.VideosAdapter;

/**
 * File created by josiassena on 7/17/16.
 */
public enum Injector {

    INSTANCE;

    private SSSComponent sssComponent;

    public void initComponent(Application application) {
        sssComponent = DaggerSSSComponent.builder()
                .appModule(new AppModule(application))
                .netModule(new NetModule())
                .permissionHelperModule(new PermissionHelperModule())
                .fireBaseRemoteConfigManagerModule(new FireBaseRemoteConfigManagerModule())
                .build();

    }

    public void inject(App app) {
        sssComponent.inject(app);
    }

    public void inject(MainPresenterImpl mainPresenter) {
        sssComponent.inject(mainPresenter);
    }

    public void inject(DownloadedVideosViewImpl downloadedVideosView) {
        sssComponent.inject(downloadedVideosView);
    }

    public void inject(VideosAdapter videosAdapter) {
        sssComponent.inject(videosAdapter);
    }

    public void inject(MainActivity mainView) {
        sssComponent.inject(mainView);
    }

    public void inject(VideoViewImpl videoView) {
        sssComponent.inject(videoView);
    }

    public void inject(FireBaseRemoteConfigManager fireBaseRemoteConfigManager) {
        sssComponent.inject(fireBaseRemoteConfigManager);
    }
}
