package com.josiassena.supersimplesongs.helpers.firebase;

/**
 * Holds all FireBase remote config key constants
 * <p>
 * File created by josiassena on 10/7/16.
 */
final class FBRemoteConfigConstants {

    private FBRemoteConfigConstants() {
        // should not be initialized
    }

    static final String MAIN_BACKGROUND_COLOR = "main_bg_color";
    static final String TOOLBAR_COLOR = "toolbar_color";
    static final String NAVIGATION_DRAWER_BG_COLOR = "navigation_drawer_bg_color";

}
