package com.josiassena.supersimplesongs.helpers;

import android.app.Activity;

/**
 * File created by josiassena on 10/10/16.
 */

public class VideoDownloadManager {

    private final Activity activity;

    public VideoDownloadManager(Activity activity) {
        this.activity = activity;
    }

/*    public void downloadVideo(final Video video) {
        YouTubeUriExtractor ytEx = new YouTubeUriExtractor(activity) {
            @Override
            public void onUrisAvailable(String videoId, String videoTitle,
                                        SparseArray<YtFile> ytFiles) {
                if (ytFiles != null && ytFiles.size() > 0) {

                    // see https://en.wikipedia.org/wiki/YouTube#Quality_and_formats
                    int MP4_720 = 22;
                    int MP4_360 = 18;
                    int FLV_240 = 5;
                    int _3GP_240 = 36;
                    int _3GP_144 = 17;

                    YtFile ytFile = ytFiles.get(MP4_720);

                    if (ytFile == null) {
                        ytFile = ytFiles.get(MP4_360);
                    }

                    if (ytFile == null) {
                        ytFile = ytFiles.get(FLV_240);
                    }

                    if (ytFile == null) {
                        ytFile = ytFiles.get(_3GP_240);
                    }

                    if (ytFile == null) {
                        ytFile = ytFiles.get(_3GP_144);
                    }

                    if (ytFile != null) {
                        String downloadUrl = ytFile.getUrl();
                        String title = video.getSnippet().getTitle();

                        download(downloadUrl, title,
                                video.getSnippet().getResourceId().getVideoId() + "---" + title);

                    }
                } else {
                    Log.e(TAG, "onUrisAvailable: NOTHING TO DOWNLOAD");
                }
            }
        };

        ytEx.execute(YouTubeBaseUrl.URL + video.getSnippet().getResourceId().getVideoId());
    }

    private void download(String youtubeDlUrl, String downloadTitle, String fileName) {
        Uri uri = Uri.parse(youtubeDlUrl);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setTitle(downloadTitle);
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(
                DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Constants.VIDEO_DOWNLOAD_DIRECTORY, fileName);

        // get downloadVideo service and enqueue file
        DownloadManager manager = (DownloadManager) activity
                .getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    */
}
