package com.josiassena.supersimplesongs.helpers.firebase;

import android.graphics.Color;
import android.support.annotation.ColorInt;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import static com.josiassena.supersimplesongs.helpers.firebase.FBRemoteConfigConstants.MAIN_BACKGROUND_COLOR;
import static com.josiassena.supersimplesongs.helpers.firebase.FBRemoteConfigConstants.NAVIGATION_DRAWER_BG_COLOR;
import static com.josiassena.supersimplesongs.helpers.firebase.FBRemoteConfigConstants.TOOLBAR_COLOR;

/**
 * Provides all firebase UI related configurations
 * <p>
 * File created by josiassena on 10/8/16.
 */
public class FireBaseUIConfigProvider {

    private FirebaseRemoteConfig firebaseRemoteConfig;

    public FireBaseUIConfigProvider() {
        if (firebaseRemoteConfig == null) {
            this.firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        }
    }

    @ColorInt
    public int getMainBackgroundColor() {
        return Color.parseColor(firebaseRemoteConfig.getString(MAIN_BACKGROUND_COLOR));
    }

    @ColorInt
    public int getToolbarColor() {
        return Color.parseColor(firebaseRemoteConfig.getString(TOOLBAR_COLOR));
    }

    @ColorInt
    public int getNavDrawerBgColor() {
        return Color.parseColor(firebaseRemoteConfig.getString(NAVIGATION_DRAWER_BG_COLOR));
    }

}
