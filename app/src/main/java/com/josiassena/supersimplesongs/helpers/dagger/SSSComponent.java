package com.josiassena.supersimplesongs.helpers.dagger;

import com.josiassena.supersimplesongs.App;
import com.josiassena.supersimplesongs.downloaded_videos.view.DownloadedVideosViewImpl;
import com.josiassena.supersimplesongs.helpers.firebase.FireBaseRemoteConfigManager;
import com.josiassena.supersimplesongs.main.presenter.MainPresenterImpl;
import com.josiassena.supersimplesongs.main.view.MainActivity;
import com.josiassena.supersimplesongs.videos.view.VideoViewImpl;
import com.josiassena.supersimplesongs.videos.view.rec_view.VideosAdapter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * File created by josiassena on 7/17/16.
 */
@Singleton
@Component (modules = {AppModule.class, NetModule.class, PermissionHelperModule.class,
        FireBaseRemoteConfigManagerModule.class})
interface SSSComponent {

    void inject(App app);

    void inject(MainPresenterImpl mainPresenter);

    void inject(DownloadedVideosViewImpl downloadedVideosView);

    void inject(VideosAdapter videosAdapter);

    void inject(MainActivity mainView);

    void inject(VideoViewImpl videoView);

    void inject(FireBaseRemoteConfigManager fireBaseRemoteConfigManager);
}
