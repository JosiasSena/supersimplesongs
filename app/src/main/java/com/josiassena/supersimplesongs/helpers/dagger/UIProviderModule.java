package com.josiassena.supersimplesongs.helpers.dagger;

import com.josiassena.supersimplesongs.helpers.firebase.FireBaseUIConfigProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * File created by josiassena on 10/8/16.
 */
@Module
class UIProviderModule {

    @Provides
    @Singleton
    FireBaseUIConfigProvider providesUIProvider() {
        return new FireBaseUIConfigProvider();
    }

}
