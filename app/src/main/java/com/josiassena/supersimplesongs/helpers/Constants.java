package com.josiassena.supersimplesongs.helpers;

import android.os.Environment;

/**
 * File created by josiassena on 7/17/16.
 */
public class Constants {

    /** The request code when calling startActivityForResult to recover from an API service error. */
    public static final int RECOVERY_DIALOG_REQUEST = 1;
    public static final int REQ_START_STANDALONE_PLAYER = 2;
    public static final int REQ_RESOLVE_SERVICE_MISSING = 3;

    public static final String PLAYLIST = "play_list";
    public static final String PLAYLIST_TITLE = "play_list_title";
    public static final String VIDEO_DOWNLOAD_DIRECTORY =
            Environment.DIRECTORY_DOWNLOADS + "/SuperSimpleSongs";
}
