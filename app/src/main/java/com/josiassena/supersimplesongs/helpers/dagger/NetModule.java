package com.josiassena.supersimplesongs.helpers.dagger;

import android.app.Application;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.josiassena.supersimplesongs.helpers.api.SSSApi;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.BaseUrl;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * File created by josiassena on 7/17/16.
 */
@Module
class NetModule {

    @Provides
    @Singleton
    Cache providesOkHttpCache(final Application application) {
        final int cacheSize = 12 * 1024 * 1024;
        return new Cache(application.getCacheDir(), cacheSize);

    }

    @Provides
    @Singleton
    Gson providesGson() {
        final GsonBuilder gsonBuilder = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);

        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient(final Cache cache) {
        final OkHttpClient client = new OkHttpClient();
        client.setCache(cache);
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(10, TimeUnit.SECONDS);
        return client;
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(final Gson gson, final OkHttpClient okHttpClient) {
        final BaseUrl url = new BaseUrl() {
            @Override
            public HttpUrl url() {
                return HttpUrl.parse("https://www.googleapis.com/youtube/v3/");
            }
        };

        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(url)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    SSSApi providesSSSApi(final Retrofit retrofit) {
        return retrofit.create(SSSApi.class);
    }

}