package com.josiassena.supersimplesongs.helpers.dagger;

import com.josiassena.supersimplesongs.helpers.firebase.FireBaseRemoteConfigManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * File created by josiassena on 10/7/16.
 */
@Module
class FireBaseRemoteConfigManagerModule {

    @Provides
    @Singleton
    FireBaseRemoteConfigManager providesFireBaseRemoteConfigManager() {
        return FireBaseRemoteConfigManager.INSTANCE;
    }

}
