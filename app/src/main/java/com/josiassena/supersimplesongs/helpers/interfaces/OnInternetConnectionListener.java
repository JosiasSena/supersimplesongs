package com.josiassena.supersimplesongs.helpers.interfaces;

import android.app.Activity;

/**
 * File created by josiassena on 9/11/16.
 */
public interface OnInternetConnectionListener {
    void checkForInternet(Activity activity);
}
