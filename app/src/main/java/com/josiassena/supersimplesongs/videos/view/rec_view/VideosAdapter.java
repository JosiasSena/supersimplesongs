package com.josiassena.supersimplesongs.videos.view.rec_view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.josiassena.supersimplesongs.BuildConfig;
import com.josiassena.supersimplesongs.R;
import com.josiassena.supersimplesongs.helpers.AdManager;
import com.josiassena.supersimplesongs.helpers.Constants;
import com.josiassena.supersimplesongs.helpers.DeveloperKey;
import com.josiassena.supersimplesongs.helpers.PermissionHelper;
import com.josiassena.supersimplesongs.helpers.dagger.Injector;
import com.josiassena.supersimplesongs.models.Snippet;
import com.josiassena.supersimplesongs.models.Video;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * File created by josiassena on 7/17/16.
 */
public class VideosAdapter extends RecyclerView.Adapter<VideosViewHolder> {

    private static final String TAG = VideosAdapter.class.getSimpleName();

    private static final int AD_TYPE = 16;

    @Inject
    PermissionHelper permissionHelper;

    private final Activity activity;
    private ArrayList<Video> videos = new ArrayList<>();

    public VideosAdapter(Activity activity, ArrayList<Video> videos) {
        this.activity = activity;
        this.videos = videos;

        Injector.INSTANCE.inject(this);
    }

    @Override
    public VideosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == AD_TYPE && !BuildConfig.DEBUG) {
            return new VideosViewHolder(
                    LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.video_ad_view, parent, false));
        } else {
            return new VideosViewHolder(LayoutInflater.from(activity)
                    .inflate(R.layout.videos_item_view, parent, false));
        }
    }

    @SuppressWarnings ("ConstantConditions")
    @Override
    public void onBindViewHolder(final VideosViewHolder holder, int position) {

        if (getItemViewType(position) == AD_TYPE && !BuildConfig.DEBUG) {
            holder.adView.loadAd(AdManager.INSTANCE.getAdRequest());
        } else {
            final Video video = videos.get(position);
            final Snippet snippet = video.getSnippet();

            holder.cardToolbar.setTitle(snippet.getTitle());
            holder.videoDescription.setText(snippet.getDescription());

            holder.videoThumbnail.setTag(video.getEtag());
            holder.videoThumbnail.initialize(DeveloperKey.DEVELOPER_KEY,
                    new YouTubeThumbnailView.OnInitializedListener() {

                        @Override
                        public void onInitializationSuccess(
                                YouTubeThumbnailView youTubeThumbnailView,
                                final YouTubeThumbnailLoader youTubeThumbnailLoader) {
                            youTubeThumbnailLoader.setVideo(snippet.getResourceId().getVideoId());

                            releaseLoader(youTubeThumbnailLoader);
                        }

                        @Override
                        public void onInitializationFailure(
                                YouTubeThumbnailView youTubeThumbnailView,
                                YouTubeInitializationResult youTubeInitializationResult) {
                            holder.videoThumbnail.setImageResource(R.drawable.no_thumbnail);
                        }
                    });

            holder.showMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    displayMoreInfoDialog(snippet);
                }
            });

            setClickListener(holder, snippet);
        }
    }

    private void releaseLoader(final YouTubeThumbnailLoader youTubeThumbnailLoader) {
        youTubeThumbnailLoader.setOnThumbnailLoadedListener(
                new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                    @Override
                    public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView,
                                                  String s) {
                        youTubeThumbnailLoader.release();
                    }

                    @Override
                    public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView,
                                                 YouTubeThumbnailLoader.ErrorReason errorReason) {
                        youTubeThumbnailLoader.release();
                    }
                });
    }

    private void setClickListener(final VideosViewHolder holder, final Snippet snippet) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openYouTubePlayer(snippet, holder.getAdapterPosition());
            }
        });
    }

    private void openYouTubePlayer(Snippet snippet, int position) {
        Log.d(TAG, "openYouTubePlayer() was called");

        Intent intent = YouTubeStandalonePlayer.createPlaylistIntent(activity,
                DeveloperKey.DEVELOPER_KEY, snippet.getPlaylistId(), position, 0, true, false);

        if (intent != null) {
            if (canResolveIntent(intent)) {
                activity.startActivity(intent);
            } else {
                // Could not resolve the intent - must need to install or update the
                // YouTube API service.
                YouTubeInitializationResult.SERVICE_MISSING.getErrorDialog(activity,
                        Constants.REQ_RESOLVE_SERVICE_MISSING).show();
            }
        }
    }

    private void displayMoreInfoDialog(Snippet snippet) {
        Log.d(TAG, "displayMoreInfoDialog() was called");

        new AlertDialog.Builder(activity)
                .setCancelable(false)
                .setMessage(snippet.getDescription())
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private boolean canResolveIntent(Intent intent) {
        Log.d(TAG, "canResolveIntent() was called");

        List<ResolveInfo> resolveInfo = activity.getPackageManager()
                .queryIntentActivities(intent, 0);
        return resolveInfo != null && !resolveInfo.isEmpty();
    }

    /**
     * Returns the total number of items in the data set hold by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return (null != videos ? videos.size() : 0);
    }

    @Override
    public int getItemViewType(final int position) {
        if ((position != 0) && ((position % 4) == 0) && !BuildConfig.DEBUG) {
            return AD_TYPE;
        } else {
            return super.getItemViewType(position);
        }
    }
}
