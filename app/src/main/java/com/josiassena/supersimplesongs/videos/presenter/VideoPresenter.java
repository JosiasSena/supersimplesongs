package com.josiassena.supersimplesongs.videos.presenter;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.josiassena.supersimplesongs.videos.view.VideoView;

/**
 * File created by josiassena on 7/17/16.
 */
interface VideoPresenter extends MvpPresenter<VideoView> {
}
