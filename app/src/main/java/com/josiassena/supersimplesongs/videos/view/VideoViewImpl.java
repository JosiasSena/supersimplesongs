package com.josiassena.supersimplesongs.videos.view;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.josiassena.supersimplesongs.R;
import com.josiassena.supersimplesongs.helpers.AdManager;
import com.josiassena.supersimplesongs.helpers.Constants;
import com.josiassena.supersimplesongs.helpers.PermissionHelper;
import com.josiassena.supersimplesongs.helpers.dagger.Injector;
import com.josiassena.supersimplesongs.models.Video;
import com.josiassena.supersimplesongs.videos.presenter.VideoPresenterImpl;
import com.josiassena.supersimplesongs.videos.view.rec_view.VideosAdapter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * File created by josiassena on 7/17/16.
 */
public class VideoViewImpl extends MvpActivity<VideoView, VideoPresenterImpl> implements VideoView {

    private static final String TAG = VideoViewImpl.class.getSimpleName();

    @BindView (R.id.toolbar)
    Toolbar toolbar;

    @Inject
    PermissionHelper permissionHelper;

    private ArrayList<Video> videos;

    /**
     * Instantiate a presenter instance
     *
     * @return The {@link MvpPresenter} for this view
     */
    @NonNull
    @Override
    public VideoPresenterImpl createPresenter() {
        return new VideoPresenterImpl();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos);
        ButterKnife.bind(this);
        Injector.INSTANCE.inject(this);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        getVideos();

        initRecView();

        AdManager.INSTANCE.displayInterstitialAd();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                switch (permissions[0]) {
                    case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                        displayPermissionNeedDialog();
                        break;
                    default:
                        Log.e(TAG, "default block of permissions result");
                        break;
                }
            }
        }
    }

    private void displayPermissionNeedDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.storage_perm_req)
                .setMessage(R.string.enable_storage_permission)
                .setPositiveButton(R.string.word_ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                permissionHelper.goToAppSettings(VideoViewImpl.this);
                            }
                        })
                .setNegativeButton(R.string.word_cancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
    }

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.setting_downloaded_videos) {
            startActivity(new Intent(this, DownloadedVideosViewImpl.class));
        }

        return super.onOptionsItemSelected(item);
    }*/

    private void getVideos() {
        Log.d(TAG, "getVideos() was called");

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            videos = extras.getParcelableArrayList(Constants.PLAYLIST);

            String playListTitle = extras.getString(Constants.PLAYLIST_TITLE);
            toolbar.setSubtitle(playListTitle);
        }
    }

    private void initRecView() {
        Log.d(TAG, "initRecView() was called");

        RecyclerView rvVideos = ButterKnife.findById(this, R.id.rv_videos);
        rvVideos.setItemViewCacheSize(50);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvVideos.setLayoutManager(layoutManager);

        VideosAdapter adapter = new VideosAdapter(this, videos);
        rvVideos.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQ_START_STANDALONE_PLAYER && resultCode != RESULT_OK) {
            YouTubeInitializationResult errorReason = YouTubeStandalonePlayer
                    .getReturnedInitializationResult(data);

            if (errorReason.isUserRecoverableError()) {
                errorReason.getErrorDialog(this, 0).show();
            } else {
                displayErrorDialog(errorReason);
            }
        }
    }

    private void displayErrorDialog(YouTubeInitializationResult errorReason) {
        String errorMessage = String.format(getString(R.string.error_player),
                errorReason.toString());

        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setMessage(errorMessage)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }
}