package com.josiassena.supersimplesongs.videos.view.rec_view;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.josiassena.supersimplesongs.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * File created by josiassena on 7/17/16.
 */
class VideosViewHolder extends RecyclerView.ViewHolder {

    @Nullable
    @BindView (R.id.card_toolbar)
    Toolbar cardToolbar;

    @Nullable
    @BindView(R.id.video_thumbnail)
    YouTubeThumbnailView videoThumbnail;

    @Nullable
    @BindView(R.id.tv_video_description)
    TextView videoDescription;

    @Nullable
    @BindView(R.id.tv_show_more)
    TextView showMore;

    @Nullable
    @BindView (R.id.ad_view)
    NativeExpressAdView adView;

    VideosViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
