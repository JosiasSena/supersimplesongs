package com.josiassena.supersimplesongs.videos.presenter;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.josiassena.supersimplesongs.videos.view.VideoView;

/**
 * File created by josiassena on 7/17/16.
 */
public class VideoPresenterImpl extends MvpBasePresenter<VideoView> implements VideoPresenter {

}
