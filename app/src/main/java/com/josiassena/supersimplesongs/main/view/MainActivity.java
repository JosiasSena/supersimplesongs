package com.josiassena.supersimplesongs.main.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.ads.NativeExpressAdView;
import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.josiassena.supersimplesongs.BuildConfig;
import com.josiassena.supersimplesongs.R;
import com.josiassena.supersimplesongs.helpers.AdManager;
import com.josiassena.supersimplesongs.helpers.Constants;
import com.josiassena.supersimplesongs.helpers.PermissionHelper;
import com.josiassena.supersimplesongs.helpers.dagger.Injector;
import com.josiassena.supersimplesongs.main.presenter.MainPresenterImpl;
import com.josiassena.supersimplesongs.main.view.rec_view.PlayListAdapter;
import com.josiassena.supersimplesongs.models.PlayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends MvpActivity<MainView, MainPresenterImpl> implements MainView {

    private static final String TAG = MainActivity.class.getSimpleName();

    @BindView (R.id.toolbar)
    Toolbar toolbar;

    @BindView (R.id.offline_view)
    LinearLayout offlineView;

    @BindView (R.id.rv_playlists)
    RecyclerView rvPlayLists;

    @BindView (R.id.ad_view)
    NativeExpressAdView adView;

    @Inject
    PermissionHelper permissionHelper;

    private boolean leaveApp = false;

    private PlayListAdapter adapter;
    private ProgressDialog dialog;
    private AdManager adManager;

    /**
     * Instantiate a presenter instance
     *
     * @return The {@link MvpPresenter} for this view
     */
    @NonNull
    @Override
    public MainPresenterImpl createPresenter() {
        return new MainPresenterImpl();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Injector.INSTANCE.inject(this);

        setSupportActionBar(toolbar);

        askForPermissions();

        initDialog();

        adManager = AdManager.INSTANCE;
        adManager.initAds(this);

        showAdView();

        presenter.checkForInternet(this);
    }

    private void showAdView() {
        if (!BuildConfig.DEBUG) {
            if (adView != null) {
                adView.setVisibility(View.VISIBLE);
                adView.loadAd(adManager.getAdRequest());
            }
        }
    }

    private void initDialog() {
        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.please_wait_dots));
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
    }

    private void askForPermissions() {
        Log.d(TAG, "askForPermissions() was called");

        String[] permissions = permissionHelper.getPermissions();

        for (String permission : permissions) {
            if (!permissionHelper.isPermissionGranted(this, permission)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(permissionHelper.getPermissions(), 929);
                }
            }
        }
    }

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.setting_downloaded_videos) {
            startActivity(new Intent(this, DownloadedVideosViewImpl.class));
        }

        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void initRecView() {
        Log.d(TAG, "initRecView() was called");

        adapter = new PlayListAdapter(this);

        showAdView();
        rvPlayLists.setVisibility(View.VISIBLE);
        offlineView.setVisibility(View.GONE);

        rvPlayLists.setItemViewCacheSize(50);
        rvPlayLists.setLayoutManager(new GridLayoutManager(this, 2));
        rvPlayLists.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RECOVERY_DIALOG_REQUEST) {
            // Recreate the activity if user performed a recovery action
            recreate();
        }
    }

    @Override
    public void onGotPlayList(PlayList playList) {
        Log.d(TAG, "onGotPlayList() was called");
        adapter.addPlayList(playList);
    }

    @Override
    public void displayOfflineWarning() {
        Snackbar.make(toolbar, R.string.no_internet, Snackbar.LENGTH_LONG).show();

        hideAdView();
        rvPlayLists.setVisibility(View.GONE);
        offlineView.setVisibility(View.VISIBLE);
    }

    private void hideAdView() {
        if (adView != null) {
            adView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showLoading() {
        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void hideLoading() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @OnClick (R.id.btn_try_again)
    public void onClick() {
        presenter.checkForInternet(this);
    }

    @Override
    public void onBackPressed() {
        if (leaveApp) {
            exitApplication();
        } else {
            Toast.makeText(this, R.string.exit_app, Toast.LENGTH_SHORT).show();
            leaveApp = true; // turn exit to true so that we can exit

            // Wait 3 seconds, if the back button is pressed again
            // within 3 seconds then we exit the app
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    leaveApp = false;
                }
            }, 3000);
        }
    }

    public void exitApplication() {
        Process.killProcess(Process.myPid());
        System.exit(1);
    }
}
