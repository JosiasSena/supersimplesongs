package com.josiassena.supersimplesongs.main.presenter;

import android.app.Activity;
import android.net.NetworkInfo;
import android.util.Log;

import com.github.pwittchen.reactivenetwork.library.Connectivity;
import com.github.pwittchen.reactivenetwork.library.ReactiveNetwork;
import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.josiassena.supersimplesongs.R;
import com.josiassena.supersimplesongs.helpers.Constants;
import com.josiassena.supersimplesongs.helpers.DeveloperKey;
import com.josiassena.supersimplesongs.helpers.api.SSSApi;
import com.josiassena.supersimplesongs.helpers.dagger.Injector;
import com.josiassena.supersimplesongs.helpers.interfaces.OnInternetConnectionListener;
import com.josiassena.supersimplesongs.main.view.MainView;
import com.josiassena.supersimplesongs.models.PlayList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * File created by josiassena on 7/17/16.
 */
public class MainPresenterImpl extends MvpBasePresenter<MainView> implements MainPresenter,
        OnInternetConnectionListener {

    private static final String TAG = MainPresenterImpl.class.getSimpleName();

    @Inject
    SSSApi sssApi;

    private List<String> playListIds;

    public MainPresenterImpl() {
        Injector.INSTANCE.inject(this);
    }

    @Override
    public void checkYouTubeApi(Activity activity) {
        YouTubeInitializationResult errorReason = YouTubeApiServiceUtil
                .isYouTubeApiServiceAvailable(activity);

        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(activity, Constants.RECOVERY_DIALOG_REQUEST).show();
        } else if (errorReason != YouTubeInitializationResult.SUCCESS) {
            String errorMessage = String
                    .format(activity.getString(R.string.error_player), errorReason.toString());
            Log.e(TAG, "checkYouTubeApi: " + errorMessage);
        }
    }

    @Override
    public void getPlayLists() {
        Log.d(TAG, "getPlayLists() was called");

        for (final String playListId : playListIds) {
            getPlayList(playListId);
        }
    }

    private void getPlayList(final String playListId) {
        sssApi.getPlayList(playListId, DeveloperKey.DEVELOPER_KEY)
                .enqueue(new Callback<PlayList>() {
                    @Override
                    public void onResponse(Response<PlayList> response, Retrofit retrofit) {
                        if (response.isSuccess()) {
                            getPlayListInfo(playListId, response.body());
                        } else {

                            if (getView() != null && isViewAttached()) {
                                getView().hideLoading();
                            }

                            try {
                                Log.e(TAG, "onResponse error --> " +
                                        response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.e(TAG, "getPlayList onFailure() called with: " +
                                "t = [" + t.getMessage() + "]", t);

                        if (getView() != null && isViewAttached()) {
                            getView().hideLoading();
                        }
                    }
                });
    }

    @Override
    public void getPlaylistIdsFromFireBaseDatabase() {
        Log.d(TAG, "getPlaylistIdsFromFireBaseDatabase() was called");

        playListIds = new ArrayList<>();

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("playListIds");

        // Read from the database
        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG,
                        "onChildAdded() called with: dataSnapshot = [" + dataSnapshot.getValue() +
                                "]");

                getPlayList((String) dataSnapshot.getValue());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG,
                        "onChildChanged() called with: dataSnapshot = [" + dataSnapshot.getValue() +
                                "], previousChildName = [" + previousChildName + "]");

                updatePlayListIds(dataSnapshot);
                getPlayLists();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d(TAG,
                        "onChildRemoved() called with: dataSnapshot = [" + dataSnapshot.getValue() +
                                "]");

                String value = (String) dataSnapshot.getValue();
                playListIds.remove(value);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG,
                        "onChildMoved() called with: dataSnapshot = [" + dataSnapshot.getValue() +
                                "], previousChildName = [" + previousChildName + "]");

                updatePlayListIds(dataSnapshot);
                getPlayLists();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "onCancelled() was called" + databaseError.toString());
            }
        });
    }

    private void updatePlayListIds(DataSnapshot dataSnapshot) {
        Log.d(TAG, "updatePlayListIds() was called");

        Iterable<DataSnapshot> list = dataSnapshot.getChildren();

        for (DataSnapshot aList : list) {
            playListIds.add((String) aList.getValue());
        }

        printPlayLists();

        getPlayLists();
    }

    private void printPlayLists() {
        Log.e(TAG, "updatePlayListIds: " + playListIds.size());

        for (String playList : playListIds) {
            Log.d(TAG, "updatePlayListIds: " + playList);
        }
    }

    private void getPlayListInfo(String playListId, final PlayList playList) {
        sssApi.getPlayListInfo(playListId, DeveloperKey.DEVELOPER_KEY)
                .enqueue(new Callback<PlayList>() {
                    @Override
                    public void onResponse(Response<PlayList> response, Retrofit retrofit) {
                        if (response.isSuccess()) {
                            playList.setPlayListTitle(
                                    response.body().getVideos().get(0).getSnippet().getTitle());

                            if (getView() != null && isViewAttached()) {
                                getView().onGotPlayList(playList);
                            }

                        } else {
                            try {
                                Log.e(TAG, "onResponse error --> " + response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        if (getView() != null && isViewAttached()) {
                            getView().hideLoading();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.e(TAG, "getPlayListInfo onFailure() called with: " + "t = [" +
                                t.getMessage() + "]", t);

                        if (getView() != null && isViewAttached()) {
                            getView().hideLoading();
                        }
                    }
                });
    }

    @Override
    public void checkForInternet(final Activity activity) {
        ReactiveNetwork.observeNetworkConnectivity(activity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .distinctUntilChanged()
                .subscribe(new Action1<Connectivity>() {
                    @Override
                    public void call(Connectivity connectivity) {
                        Log.d(TAG,
                                "call() called with: " + "connectivity = [" +
                                        connectivity.toString() + "]");

                        if (getView() != null && isViewAttached()) {
                            getView().showLoading();
                        }

                        if (isOffline(connectivity)) {

                            if (getView() != null && isViewAttached()) {
                                getView().displayOfflineWarning();
                                getView().hideLoading();
                            }

                        } else {
                            if (getView() != null && isViewAttached()) {
                                getView().initRecView();
                            }

                            getPlaylistIdsFromFireBaseDatabase();

                            checkYouTubeApi(activity);
                        }
                    }
                });
    }

    private boolean isOffline(final Connectivity connectivity) {
        return connectivity.getState().equals(NetworkInfo.State.DISCONNECTING) ||
                connectivity.getState().equals(NetworkInfo.State.DISCONNECTED) ||
                connectivity.getState().equals(NetworkInfo.State.UNKNOWN);
    }
}
