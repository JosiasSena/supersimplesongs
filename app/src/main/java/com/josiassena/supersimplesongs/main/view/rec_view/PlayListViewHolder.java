package com.josiassena.supersimplesongs.main.view.rec_view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeThumbnailView;
import com.josiassena.supersimplesongs.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * File created by josiassena on 7/17/16.
 */
class PlayListViewHolder extends RecyclerView.ViewHolder {

    @BindView (R.id.tv_play_list_title)
    TextView playListTitle;

    @BindView (R.id.play_list_thumbnail)
    YouTubeThumbnailView playListThumbnail;

    PlayListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
