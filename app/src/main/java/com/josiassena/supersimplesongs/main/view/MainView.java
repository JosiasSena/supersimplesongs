package com.josiassena.supersimplesongs.main.view;

import com.hannesdorfmann.mosby.mvp.MvpView;
import com.josiassena.supersimplesongs.models.PlayList;

/**
 * File created by josiassena on 7/17/16.
 */
public interface MainView extends MvpView {
    void initRecView();

    void onGotPlayList(PlayList playList);

    void displayOfflineWarning();

    void showLoading();

    void hideLoading();
}
