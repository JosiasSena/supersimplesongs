package com.josiassena.supersimplesongs.main.view.rec_view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.josiassena.supersimplesongs.R;
import com.josiassena.supersimplesongs.helpers.Constants;
import com.josiassena.supersimplesongs.helpers.DeveloperKey;
import com.josiassena.supersimplesongs.models.PlayList;
import com.josiassena.supersimplesongs.models.Video;
import com.josiassena.supersimplesongs.videos.view.VideoViewImpl;

import java.util.ArrayList;

/**
 * File created by josiassena on 7/17/16.
 */
public class PlayListAdapter extends RecyclerView.Adapter<PlayListViewHolder> {

    private final Context context;
    private final ArrayList<PlayList> playLists = new ArrayList<>();

    public PlayListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public PlayListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PlayListViewHolder(
                LayoutInflater.from(context).inflate(R.layout.playlist_item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(final PlayListViewHolder holder, int position) {
        final PlayList playList = playLists.get(position);

        holder.playListThumbnail.setTag(playList.getVideos().get(0).getEtag());
        holder.playListTitle.setText(playList.getPlayListTitle());

        initThumbnail(holder, playList);

        setClickListener(holder, playList);
    }

    private void setClickListener(PlayListViewHolder holder, final PlayList playList) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, VideoViewImpl.class);
                intent.putExtra(Constants.PLAYLIST_TITLE, playList.getPlayListTitle());
                intent.putParcelableArrayListExtra(Constants.PLAYLIST,
                        (ArrayList<Video>) playList.getVideos());

                context.startActivity(intent);
            }
        });
    }

    private void initThumbnail(final PlayListViewHolder holder, final PlayList playList) {
        holder.playListThumbnail.initialize(DeveloperKey.DEVELOPER_KEY,
                new YouTubeThumbnailView.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView,
                                                        YouTubeThumbnailLoader thumbnailLoader) {
                        thumbnailLoader.setPlaylist(
                                playList.getVideos().get(0).getSnippet().getPlaylistId());

                        releaseLoader(thumbnailLoader);
                    }

                    @Override
                    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView,
                                                        YouTubeInitializationResult result) {
                        holder.playListThumbnail.setImageResource(R.drawable.no_thumbnail);
                    }
                });
    }

    private void releaseLoader(final YouTubeThumbnailLoader youTubeThumbnailLoader) {
        youTubeThumbnailLoader.setOnThumbnailLoadedListener(
                new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                    @Override
                    public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView,
                                                  String s) {
                        youTubeThumbnailLoader.release();
                    }

                    @Override
                    public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView,
                                                 YouTubeThumbnailLoader.ErrorReason errorReason) {
                        youTubeThumbnailLoader.release();
                    }
                });
    }

    /**
     * Returns the total number of items in the data set hold by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return (null != playLists ? playLists.size() : 0);
    }

    public void addPlayList(PlayList playList) {

        if (!playLists.contains(playList)) {
            playLists.add(playList);
            notifyItemInserted(playLists.size() - 1);
        }
    }
}
