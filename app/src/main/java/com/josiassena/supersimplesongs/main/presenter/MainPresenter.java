package com.josiassena.supersimplesongs.main.presenter;

import android.app.Activity;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.josiassena.supersimplesongs.main.view.MainView;

/**
 * File created by josiassena on 7/17/16.
 */
interface MainPresenter extends MvpPresenter<MainView> {

    void checkYouTubeApi(Activity activity);

    void getPlayLists();

    void getPlaylistIdsFromFireBaseDatabase();
}
