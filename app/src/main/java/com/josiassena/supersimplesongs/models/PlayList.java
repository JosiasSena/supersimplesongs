package com.josiassena.supersimplesongs.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

/**
 * File created by josiassena on 7/17/16.
 */
@Generated ("org.jsonschema2pojo")
public class PlayList implements Parcelable {

    private String playListTitle;

    @SerializedName ("kind")
    @Expose
    private String kind;

    @SerializedName ("etag")
    @Expose
    private String etag;

    @SerializedName ("pageInfo")
    @Expose
    private PageInfo pageInfo;

    @SerializedName ("items")
    @Expose
    private List<Video> videos = new ArrayList<Video>();

    private PlayList(Parcel in) {
        playListTitle = in.readString();
        kind = in.readString();
        etag = in.readString();
    }

    public static final Creator<PlayList> CREATOR = new Creator<PlayList>() {
        @Override
        public PlayList createFromParcel(Parcel in) {
            return new PlayList(in);
        }

        @Override
        public PlayList[] newArray(int size) {
            return new PlayList[size];
        }
    };

    /**
     * @return The kind
     */
    public String getKind() {
        return kind;
    }

    /**
     * @param kind The kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    /**
     * @return The etag
     */
    public String getEtag() {
        return etag;
    }

    /**
     * @param etag The etag
     */
    public void setEtag(String etag) {
        this.etag = etag;
    }

    /**
     * @return The pageInfo
     */
    public PageInfo getPageInfo() {
        return pageInfo;
    }

    /**
     * @param pageInfo The pageInfo
     */
    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    /**
     * @return The items
     */
    public List<Video> getVideos() {
        return videos;
    }

    /**
     * @param videos The items
     */
    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

    public String getPlayListTitle() {
        return playListTitle;
    }

    public void setPlayListTitle(String playListTitle) {
        this.playListTitle = playListTitle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(playListTitle);
        parcel.writeString(kind);
        parcel.writeString(etag);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlayList playList = (PlayList) o;

        if (!playListTitle.equals(playList.playListTitle)) {
            return false;
        }
        if (kind != null ? !kind.equals(playList.kind) : playList.kind != null) {
            return false;
        }
        if (etag != null ? !etag.equals(playList.etag) : playList.etag != null) {
            return false;
        }
        if (pageInfo != null ? !pageInfo.equals(playList.pageInfo) : playList.pageInfo != null) {
            return false;
        }
        if (videos != null ? !videos.equals(playList.videos) : playList.videos != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = playListTitle.hashCode();
        result = 31 * result + (kind != null ? kind.hashCode() : 0);
        result = 31 * result + (etag != null ? etag.hashCode() : 0);
        result = 31 * result + (pageInfo != null ? pageInfo.hashCode() : 0);
        result = 31 * result + (videos != null ? videos.hashCode() : 0);
        return result;
    }
}