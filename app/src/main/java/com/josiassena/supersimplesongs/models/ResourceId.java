
package com.josiassena.supersimplesongs.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class ResourceId implements Parcelable {

    @SerializedName("kind")
    @Expose
    private String kind;

    @SerializedName("videoId")
    @Expose
    private String videoId;

    ResourceId(Parcel in) {
        kind = in.readString();
        videoId = in.readString();
    }

    public static final Creator<ResourceId> CREATOR = new Creator<ResourceId>() {
        @Override
        public ResourceId createFromParcel(Parcel in) {
            return new ResourceId(in);
        }

        @Override
        public ResourceId[] newArray(int size) {
            return new ResourceId[size];
        }
    };

    /**
     * @return The kind
     */
    public String getKind() {
        return kind;
    }

    /**
     * @param kind The kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    /**
     * @return The videoId
     */
    public String getVideoId() {
        return videoId;
    }

    /**
     * @param videoId The videoId
     */
    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(kind);
        parcel.writeString(videoId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ResourceId that = (ResourceId) o;

        if (kind != null ? !kind.equals(that.kind) : that.kind != null) {
            return false;
        }
        if (videoId != null ? !videoId.equals(that.videoId) : that.videoId != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = kind != null ? kind.hashCode() : 0;
        result = 31 * result + (videoId != null ? videoId.hashCode() : 0);
        return result;
    }
}
