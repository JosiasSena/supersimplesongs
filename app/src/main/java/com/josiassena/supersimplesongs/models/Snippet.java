
package com.josiassena.supersimplesongs.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Snippet implements Parcelable {

    @SerializedName("publishedAt")
    @Expose
    private String publishedAt;

    @SerializedName("channelId")
    @Expose
    private String channelId;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("thumbnails")
    @Expose
    private Thumbnails thumbnails;

    @SerializedName("channelTitle")
    @Expose
    private String channelTitle;

    @SerializedName("playlistId")
    @Expose
    private String playlistId;

    @SerializedName("position")
    @Expose
    private int position;

    @SerializedName("resourceId")
    @Expose
    private ResourceId resourceId;

    Snippet(Parcel in) {
        publishedAt = in.readString();
        channelId = in.readString();
        title = in.readString();
        description = in.readString();
        thumbnails = in.readParcelable(Thumbnails.class.getClassLoader());
        channelTitle = in.readString();
        playlistId = in.readString();
        position = in.readInt();
        resourceId = in.readParcelable(ResourceId.class.getClassLoader());
    }

    public static final Creator<Snippet> CREATOR = new Creator<Snippet>() {
        @Override
        public Snippet createFromParcel(Parcel in) {
            return new Snippet(in);
        }

        @Override
        public Snippet[] newArray(int size) {
            return new Snippet[size];
        }
    };

    /**
     * @return The publishedAt
     */
    public String getPublishedAt() {
        return publishedAt;
    }

    /**
     * @param publishedAt The publishedAt
     */
    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    /**
     * @return The channelId
     */
    public String getChannelId() {
        return channelId;
    }

    /**
     * @param channelId The channelId
     */
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The thumbnails
     */
    public Thumbnails getThumbnails() {
        return thumbnails;
    }

    /**
     * @param thumbnails The thumbnails
     */
    public void setThumbnails(Thumbnails thumbnails) {
        this.thumbnails = thumbnails;
    }

    /**
     * @return The channelTitle
     */
    public String getChannelTitle() {
        return channelTitle;
    }

    /**
     * @param channelTitle The channelTitle
     */
    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }

    /**
     * @return The playlistId
     */
    public String getPlaylistId() {
        return playlistId;
    }

    /**
     * @param playlistId The playlistId
     */
    public void setPlaylistId(String playlistId) {
        this.playlistId = playlistId;
    }

    /**
     * @return The position
     */
    public Integer getPosition() {
        return position;
    }

    /**
     * @param position The position
     */
    public void setPosition(Integer position) {
        this.position = position;
    }

    /**
     * @return The resourceId
     */
    public ResourceId getResourceId() {
        return resourceId;
    }

    /**
     * @param resourceId The resourceId
     */
    public void setResourceId(ResourceId resourceId) {
        this.resourceId = resourceId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(publishedAt);
        parcel.writeString(channelId);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeParcelable(thumbnails, i);
        parcel.writeString(channelTitle);
        parcel.writeString(playlistId);
        parcel.writeInt(position);
        parcel.writeParcelable(resourceId, i);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Snippet snippet = (Snippet) o;

        if (position != snippet.position) {
            return false;
        }
        if (publishedAt != null ? !publishedAt.equals(snippet.publishedAt) :
                snippet.publishedAt != null) {
            return false;
        }
        if (channelId != null ? !channelId.equals(snippet.channelId) : snippet.channelId != null) {
            return false;
        }
        if (title != null ? !title.equals(snippet.title) : snippet.title != null) {
            return false;
        }
        if (description != null ? !description.equals(snippet.description) :
                snippet.description != null) {
            return false;
        }
        if (thumbnails != null ? !thumbnails.equals(snippet.thumbnails) :
                snippet.thumbnails != null) {
            return false;
        }
        if (channelTitle != null ? !channelTitle.equals(snippet.channelTitle) :
                snippet.channelTitle != null) {
            return false;
        }
        if (playlistId != null ? !playlistId.equals(snippet.playlistId) :
                snippet.playlistId != null) {
            return false;
        }
        if (resourceId != null ? !resourceId.equals(snippet.resourceId) :
                snippet.resourceId != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = publishedAt != null ? publishedAt.hashCode() : 0;
        result = 31 * result + (channelId != null ? channelId.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (thumbnails != null ? thumbnails.hashCode() : 0);
        result = 31 * result + (channelTitle != null ? channelTitle.hashCode() : 0);
        result = 31 * result + (playlistId != null ? playlistId.hashCode() : 0);
        result = 31 * result + position;
        result = 31 * result + (resourceId != null ? resourceId.hashCode() : 0);
        return result;
    }
}
