package com.josiassena.supersimplesongs.models;

/**
 * File created by josiassena on 9/11/16.
 */
public class YouTubeBaseUrl {

    public static final String URL = "https://www.youtube.com/watch?v=";

}
