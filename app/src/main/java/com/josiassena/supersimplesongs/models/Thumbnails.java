
package com.josiassena.supersimplesongs.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Thumbnails implements Parcelable {

    @SerializedName("default")
    @Expose
    private Default _default;

    @SerializedName("medium")
    @Expose
    private Medium medium;

    @SerializedName("high")
    @Expose
    private High high;

    @SerializedName("standard")
    @Expose
    private Standard standard;

    @SerializedName("maxres")
    @Expose
    private Maxres maxres;

    Thumbnails(Parcel in) {
        _default = in.readParcelable(Default.class.getClassLoader());
        medium = in.readParcelable(Medium.class.getClassLoader());
        high = in.readParcelable(High.class.getClassLoader());
        standard = in.readParcelable(Standard.class.getClassLoader());
        maxres = in.readParcelable(Maxres.class.getClassLoader());
    }

    public static final Creator<Thumbnails> CREATOR = new Creator<Thumbnails>() {
        @Override
        public Thumbnails createFromParcel(Parcel in) {
            return new Thumbnails(in);
        }

        @Override
        public Thumbnails[] newArray(int size) {
            return new Thumbnails[size];
        }
    };

    /**
     * @return The _default
     */
    public Default getDefault() {
        return _default;
    }

    /**
     * @param _default The default
     */
    public void setDefault(Default _default) {
        this._default = _default;
    }

    /**
     * @return The medium
     */
    public Medium getMedium() {
        return medium;
    }

    /**
     * @param medium The medium
     */
    public void setMedium(Medium medium) {
        this.medium = medium;
    }

    /**
     * @return The high
     */
    public High getHigh() {
        return high;
    }

    /**
     * @param high The high
     */
    public void setHigh(High high) {
        this.high = high;
    }

    /**
     * @return The standard
     */
    public Standard getStandard() {
        return standard;
    }

    /**
     * @param standard The standard
     */
    public void setStandard(Standard standard) {
        this.standard = standard;
    }

    /**
     * @return The maxres
     */
    public Maxres getMaxres() {
        return maxres;
    }

    /**
     * @param maxres The maxres
     */
    public void setMaxres(Maxres maxres) {
        this.maxres = maxres;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(_default, i);
        parcel.writeParcelable(medium, i);
        parcel.writeParcelable(high, i);
        parcel.writeParcelable(standard, i);
        parcel.writeParcelable(maxres, i);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Thumbnails that = (Thumbnails) o;

        if (_default != null ? !_default.equals(that._default) : that._default != null) {
            return false;
        }
        if (medium != null ? !medium.equals(that.medium) : that.medium != null) {
            return false;
        }
        if (high != null ? !high.equals(that.high) : that.high != null) {
            return false;
        }
        if (standard != null ? !standard.equals(that.standard) : that.standard != null) {
            return false;
        }
        if (maxres != null ? !maxres.equals(that.maxres) : that.maxres != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = _default != null ? _default.hashCode() : 0;
        result = 31 * result + (medium != null ? medium.hashCode() : 0);
        result = 31 * result + (high != null ? high.hashCode() : 0);
        result = 31 * result + (standard != null ? standard.hashCode() : 0);
        result = 31 * result + (maxres != null ? maxres.hashCode() : 0);
        return result;
    }
}
