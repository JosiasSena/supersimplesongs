
package com.josiassena.supersimplesongs.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class PageInfo {

    @SerializedName("totalResults")
    @Expose
    private Integer totalResults;

    @SerializedName("resultsPerPage")
    @Expose
    private Integer resultsPerPage;

    /**
     * @return The totalResults
     */
    public Integer getTotalResults() {
        return totalResults;
    }

    /**
     * @param totalResults The totalResults
     */
    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    /**
     * @return The resultsPerPage
     */
    public Integer getResultsPerPage() {
        return resultsPerPage;
    }

    /**
     * @param resultsPerPage The resultsPerPage
     */
    public void setResultsPerPage(Integer resultsPerPage) {
        this.resultsPerPage = resultsPerPage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PageInfo pageInfo = (PageInfo) o;

        if (totalResults != null ? !totalResults.equals(pageInfo.totalResults) :
                pageInfo.totalResults != null) {
            return false;
        }
        if (resultsPerPage != null ? !resultsPerPage.equals(pageInfo.resultsPerPage) :
                pageInfo.resultsPerPage != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = totalResults != null ? totalResults.hashCode() : 0;
        result = 31 * result + (resultsPerPage != null ? resultsPerPage.hashCode() : 0);
        return result;
    }
}
