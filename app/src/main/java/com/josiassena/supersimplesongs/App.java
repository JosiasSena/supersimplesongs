package com.josiassena.supersimplesongs;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.josiassena.supersimplesongs.helpers.dagger.Injector;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * File created by josiassena on 7/17/16.
 */
public class App extends Application {

    private static final String APP_ID = "ca-app-pub-2149889302610354~7167933424";

    private FirebaseRemoteConfig firebaseRemoteConfig;

    @Override
    public void onCreate() {
        super.onCreate();

        initDagger();
        initFireBaseDatabase();
        initFireBaseRemoteConfig();

        MobileAds.initialize(this, APP_ID);
    }

    private void initFireBaseDatabase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        database.setPersistenceEnabled(true);

        DatabaseReference myRef = database.getReference("playListIds");
        myRef.keepSynced(true);
    }

    private void initDagger() {
        Injector injector = Injector.INSTANCE;
        injector.initComponent(this);
    }

    private void initFireBaseRemoteConfig() {
        Log.d(TAG, "initFireBaseRemoteConfig() was called");

        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();

        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        firebaseRemoteConfig.setConfigSettings(configSettings);
        firebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);

        performFirstFireBaseDefaultConfigFetch();
    }

    private void performFirstFireBaseDefaultConfigFetch() {
        long cacheExpiration = 3600; // 1 hour in seconds.

        // If in developer mode cacheExpiration is set to 0 so each fetch will retrieve values from
        // the server.
        if (firebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        // [START fetch_config_with_callback]
        // cacheExpirationSeconds is set to cacheExpiration here, indicating that any previously
        // fetched and cached config would be considered expired because it would have been fetched
        // more than cacheExpiration seconds ago. Thus the next fetch would go to the server unless
        // throttling is in progress. The default expiration duration is 43200 (12 hours).
        firebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.i(TAG, "FirebaseRemoteConfig fetch Succeeded!");

                            // Once the config is successfully fetched it must
                            // be activated before newly fetched values are returned.
                            firebaseRemoteConfig.activateFetched();
                        } else {
                            Log.e(TAG, "FirebaseRemoteConfig fetch failed.");
                        }
                    }
                });
        // [END fetch_config_with_callback]
    }

    @Override
    protected void attachBaseContext(final Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
